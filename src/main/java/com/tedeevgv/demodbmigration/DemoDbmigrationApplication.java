package com.tedeevgv.demodbmigration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDbmigrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDbmigrationApplication.class, args);
	}

}
